
<footer class="main-footer">
  <div class="wrapper">

    <div class="row">
      <div class="column-large-3">

        <div class="logo">
          <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/logo-grey.png" alt="">
        </div>
        <p class="copyright">All right reserved ©2017</p>

      </div>
      <div class="column-large-3">

        <div class="footer-widget widget-menu">
          <h3 class="widget-title">Products</h3>

          <ul>
            <li><a href="#">Petroleum products</a></li>
            <li><a href="#">Food grade & Industrial Oils</a></li>
            <li><a href="#">Agricultural products</a></li>
            <li><a href="#">Animal Feed Products</a></li>
            <li><a href="#">Textile Products , cotton & Synthetic Fibers</a></li>
            <li><a href="#">Detergents</a></li>
            <li><a href="#">Chemicals</a></li>
          </ul>

        </div>

      </div>
      <div class="column-large-3">

        <div class="footer-widget widget-menu">
          <h3 class="widget-title">Site Menu</h3>

          <ul>
            <li><a href="#">Home Page</a></li>
            <li><a href="#">Services</a></li>
            <li><a href="#">Products List</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Contact Us</a></li>
            <li><a href="#">Site Map</a></li>
          </ul>

        </div>

      </div>
      <div class="column-large-3">

      <div class="footer-widget widget-text">
          <h3 class="widget-title">contact</h3>

          <dl>

            <dt>Address:</dt>
            <dd>Unit 404, Setareh Building, 24 Farhad St, Mashhad, Iran</dd>

            <dt>Phone:</dt>
            <dd>051-37661788 <br> 051-37661787 <br> 051-37662534</dd>

            <dt>Email:</dt>
            <dd>
              <a href="mailto:info@sadrtrading.com">info@sadrtrading.com</a> <br> 
              <a href="mailto:trading@sadrtrading.com">trading@sadrtrading.com</a>
            </dd>

          </dl>

        </div>

      </div>
    </div>

  </div>
</footer>
    
<?php wp_footer() ?>


</body>
</html>
