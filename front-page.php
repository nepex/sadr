<?php get_header() ?>

<div class="main-slider">
  
  <div class="slides owl-carousel">

    <?php for ($i=0; $i < 3; $i++) : ?>
    <img src="<?php echo get_template_directory_uri() ?>/assets/images/header-back.jpg">
    <?php endfor ?>

  </div>

  <div class="slider-overlay">
    <h1 class="site-title"><?php bloginfo( 'site-title' ) ?></h1>
    <p class="site-desc"><?php bloginfo( 'description' ) ?></p>
    <button class="btn btn-large btn-orange btn-fancy">More information</button>

    <div class="slider-nav"></div>

  </div>
  
</div><section class="services-section">
  <div class="wrapper">
    
    <div class="row">
      
      <div class="column-large-4">
        <h2 class="title">
          <span>Our</span>
          <span>Services</span>
        </h2>
      </div>

      <div class="column-large-8">
        <div class="section-desc">

          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Veritatis cumque porro eos debitis? Reiciendis molestias ipsum iste corporis ullam, voluptates recusandae illum. Mollitia cupiditate, temporibus explicabo adipisci tenetur libero unde.</p>
          <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

          <a href="#" class="read-more"><?php _e( 'more information', 'sadr' ) ?> </a>

        </div>
      </div>

    </div>

  </div>
</section>
<div class="products-section">
  <div class="wrapper">
    
    <h2 class="section-title">product list</h2>
    
    <!-- products -->
    
    <div class="row">
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-5.jpg" alt="">
            <figcaption> Petroleum products </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-6.jpg" alt="">
            <figcaption> Food grade & Industrial Oils </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-7.jpg" alt="">
            <figcaption> Agricultural products </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-1.jpg" alt="">
            <figcaption> Animal Feed Products </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-2.jpg" alt="">
            <figcaption> Textile, cotton & Synthetic Fibers </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-3.jpg" alt="">
            <figcaption> Detergents </figcaption>
          </figure>
        </a>

      </div>

      <div class="column-large-4"></div>
      <div class="column-large-4">
        
        <a href="category.html">
          <figure class="product-block">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/.tmp/product-4.jpg" alt="">
            <figcaption> Chemicals </figcaption>
          </figure>
        </a>

      </div>
      <div class="column-large-4"></div>
    </div>

    <!-- /products -->

  </div>
</div><section class="contact-us-section">
  <div class="wrapper">
  
    <div class="contact-us-block">
      
      
      <p> Need more information on our services or products? </p>
      <a href="contact-us.html" class="btn btn-blue"> Contact us </a>
      
    
    </div>

  </div>
</section>

<?php get_footer() ?>
