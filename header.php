<?php
$langs = [];

if (function_exists( 'pll_the_languages' )) {
    $langs = pll_the_languages([ 'raw' => 1, 'hide_if_empty' => 0 ]);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title> <?php wp_title( '|', true ) ?> </title>

    <?php wp_head() ?>

</head>
<body>



<header class="main-header">

  <div class="wrapper">

    <?php the_custom_logo( ) ?>

    <div class="languages">

      <ul>
        <?php foreach ($langs as $lang) : ?>
          <li class="<?php echo $lang['slug'] ?>"> 
            <a href="<?php echo $lang['url'] ?>">
              <img src="<?php echo $lang['flag'] ?>">
                <?php echo $lang['name'] ?> 
            </a> 
          </li>
        <?php endforeach; ?>
      </ul>
    </div>


    
      
    <nav class="nav-menu">
        <?php
        wp_nav_menu([
        'container' => 'ul',
        'theme_location' => 'main-nav'
        ]);
        ?>
    </nav>

  </div>

</header>
