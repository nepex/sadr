"use strict";

jQuery(document).ready(function ($) {

  $('.owl-carousel').owlCarousel({
    items: 1,
    loop: true,
    autoplay: true,
    autoplayTimeout: 3000,
    mouseDrag: false,
    touchDrag: false,
    nav: true,
    navText: ['', ''],
    navContainer: $('.main-slider .slider-nav')
  });

});