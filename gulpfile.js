var gulp    = require('gulp');
var panini  = require('panini');
var gutil   = require('gulp-util');
var concat  = require('gulp-concat');
var uglify  = require('gulp-uglify');
var stylus  = require('gulp-stylus');
var minify  = require('gulp-minify-css');
var inject  = require('gulp-inject');
var browserSync = require('browser-sync');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('default', ['watch']);

gulp.task('watch', ['build:page', 'build:css', 'build:js','serve'], function () {
  gulp.watch('src/html/**/*.{html,json}', ['build:page']);
  gulp.watch('src/style/**/*.styl', ['build:css']);
  gulp.watch('src/js/**/*.js', ['build:js']);
});

gulp.task('serve', function () {
   browserSync.init({
     server: 'dist/'
   });
});

gulp.task('build:page', function () {
  panini.refresh();
  gulp.src('src/html/pages/**/*.html')
    .pipe(panini({
      root: 'dist/',
      data: 'src/html/data/',
      helpers: 'src/html/helpers/',
      layouts: 'src/html/layouts/',
      partials: 'src/html/partials/'
    })).on('error', handler)
    .pipe(gulp.dest('dist/'))
    .pipe(browserSync.stream());
});

gulp.task('build:css', function () {
  gulp.src('src/style/style.styl')
    .pipe(sourcemaps.init())
    .pipe(stylus()).on('error', handler)
    .pipe(sourcemaps.write('./'))
    // .pipe(minify())
    .pipe(gulp.dest('dist/assets/styles/'))
    .pipe(browserSync.stream({match: '**/*.css'}));
});

gulp.task('build:js', function () {
  gulp.src('src/js/**/*.js')
    // .pipe(uglify())
    .pipe(gulp.dest('dist/assets/scripts/'))
    .pipe(browserSync.stream());
});

gulp.task('inject', function () {

  var injectCss = gulp.src('dist/**/*.css', {read: false});
  var injectJs = gulp.src('dist/**/*.js', {read: false});

  gulp.src('.tmp/**/*.html')
    .pipe(inject(injectJs))
    .pipe(inject(injectCss))
    .pipe(gulp.dest('dist/'));
});

function handler(err) {
  return function (title) {
    this.emit('exit');
    gutil.log(gutil.colors.red('Error: '), title);
  }
}
