<?php
if (! function_exists( 'theme_setup' )) {
    
    function theme_setup()
    {
        // Custom Editor Style Support
        add_editor_style();

        // Support for Featured Images
        add_theme_support( 'post-thumbnails' );
        
        // Automatic Feed Links & Post Formats
        add_theme_support( 'automatic-feed-links' );

        // user choose the logo
        add_theme_support( 'custom-logo', ['height' => false, 'width' => false] );


        // Load Multilingual
        load_theme_textdomain( 'sadr', __DIR__ . '/languages/' );
    }
    add_action( 'after_setup_theme', 'theme_setup' );
}

if (!function_exists('init_assets')) {
    
    function init_assets()
    {
    
        $styles = [
            'google-fonts' => 'https://fonts.googleapis.com/css?family=Open+Sans:400,700|Playfair+Display:400,700',
            'app' => get_stylesheet_uri(),
            'owl-carousel' => get_template_directory_uri() . '/assets/components/owl.carousel/dist/assets/owl.carousel.min.css',
            'lightbox' => get_template_directory_uri() . '/assets/components/lightbox2/dist/css/lightbox.min.css'
        ];
    
        $scripts = [
            [
                'name'    => 'owl-carousel',
                'src'     => get_template_directory_uri() . '/assets/components/owl.carousel/dist/owl.carousel.min.js',
                'deps'    => ['jquery'],
                'version' => '2.2.1',
                'footer'  => true,
            ],
            [
                'name'    => 'lightbox',
                'src'     => get_template_directory_uri() . '/assets/components/lightbox2/dist/js/lightbox.min.js',
                'deps'    => ['jquery'],
                'version' => '2.9.0',
                'footer'  => true,
            ],
            [
                'name'    => 'app',
                'src'     => get_template_directory_uri() . '/assets/scripts/app.js',
                'deps'    => ['jquery'],
                'version' => '1.0',
                'footer'  => true,
            ],
    
        ];
    
        if (is_singular()) {
            wp_enqueue_script("comment-reply");
        }
    
        foreach ($styles as $name => $address) {
            wp_enqueue_style($name, $address);
        }
    
        foreach ($scripts as $script) {
            wp_enqueue_script(
            $script["name"],
            $script["src"],
            $script["deps"],
            $script["version"],
            $script["footer"]
            );
        }
    
        // wp_localize_script( 'app', 'passed', array( 'templateUrl' => get_stylesheet_directory_uri() ) );
    }
    
    add_action('wp_enqueue_scripts', 'init_assets');
}

if (!function_exists('full_title')) {
    
    function full_title($title, $sep = "|")
    {
    
        if (is_feed()) {
            return $title;
        }
    
        global $page,
        $paged;
    
        // Add the blog name
        $title = get_bloginfo('name', 'display') . $title;
    
        // Add the blog description for the home/front page.
        $site_description = get_bloginfo('description', 'display');
    
        if ($site_description && (is_home() || is_front_page())) {
            $title .= " $sep $site_description";
        }
    
        // Add a page number if necessary:
        if (($paged >= 2 || $page >= 2) && !is_404()) {
            $title .= " $sep " . sprintf('Page %s', max($paged, $page));
        }
    
        return $title;
    }
    
      add_filter('wp_title', 'full_title', 10, 2);
}

if (!function_exists('register_menus')) {
    
    function register_menus()
    {
    
        register_nav_menu('main-nav', __('main-nav'));
    }
    
      add_action('init', 'register_menus');
}




if (!function_exists('register_widgets')) {
    
    function register_widgets()
    {
    
        $args = [
        'name'          => __('Footer Widget area', 'sadr'),
        'id'            => 'footer-widgets',
        'before_widget' => '<div class="column-large-3"><div id="%s" class="footer-widget %s">',
        'after_widget'  => '</div></div>',
        'before_title'  => '<p class="widget-title box-title %s">',
        'after_title'   => '</p>',
        ];
    
        register_sidebar($args);
    }
    
      add_action('widgets_init', 'register_widgets');
}

if (!function_exists('pagination')) {
    
    function pagination($query = null)
    {
    
        $max_page = 0;
    
        if (isset($query)) {
            $max_page = $query->max_num_pages;
        } else {
            global $wp_query;
            $max_page = $wp_query->max_num_pages;
        }
    
        $big = 999999999;
    
    
        $links = paginate_links([
        'base'      => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
        'format'    => '?paged=%#%',
        'prev_next' => false,
        'prev_text' => '<',
        'next_text' => '>',
        'current'   => max(1, get_query_var('paged')),
        'total'     => $max_page,
        'type'      => 'list',
        ]);
    
        $pagination = str_replace('page-numbers', 'pagination', $links);
    
        echo '' . $pagination . '';
    }
    
}
